#!/bin/sh

./gradlew build
mv build/libs/courier-movement-0.0.1-SNAPSHOT.jar Docker/
cd Docker
docker-compose build --no-cache
docker-compose up timescaledb redpanda console -d
sleep 30
docker-compose up courierservice -d
rm courier-movement-0.0.1-SNAPSHOT.jar