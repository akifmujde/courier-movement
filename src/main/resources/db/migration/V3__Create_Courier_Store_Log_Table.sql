create table courier_store_log
(
    id              bigint primary key,
    courier_id      bigint    not null,
    store_id bigint    not null,
    lat             float8    not null,
    lon             float8    not null,
    time            timestamp not null,
    created_date    timestamp default now()
);

create sequence seq_courier_store_log increment 50;

create index idx_csl_courier_id_migros_store_id_time_desc on courier_store_log(courier_id, store_id, time desc);