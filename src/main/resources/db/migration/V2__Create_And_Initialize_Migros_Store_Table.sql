create table migros_store
(
    id           bigint primary key,
    name         varchar not null unique,
    lat          float8 not null,
    lon          float8 not null,
    created_date timestamp default now()
);

create sequence seq_migros_store increment 50;

insert into migros_store (id, name, lat, lon, created_date)
values (1, 'Ataşehir MMM Migros', 40.9923307, 29.1244229, now());
insert into migros_store (id, name, lat, lon, created_date)
values (2, 'Novada MMM Migros', 40.986106, 29.1161293, now());
insert into migros_store (id, name, lat, lon, created_date)
values (3, 'Beylikdüzü 5M Migros', 41.0066851, 28.6552262, now());
insert into migros_store (id, name, lat, lon, created_date)
values (4, 'Ortaköy MMM Migros', 41.055783, 29.0210292, now());
insert into migros_store (id, name, lat, lon, created_date)
values (5, 'Caddebostan MMM Migros', 40.9632463, 29.0630908, now());