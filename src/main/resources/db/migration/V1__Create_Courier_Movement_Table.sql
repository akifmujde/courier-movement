create table courier_movement
(
    id           bigint,
    courier_id   bigint      not null,
    lat          float8      not null,
    lon          float8      not null,
    time         timestamptz not null,
    created_date timestamp default now()
);

create sequence if not exists seq_courier_movement increment 50;

select create_hypertable('courier_movement', 'time', chunk_time_interval => interval '1 day', if_not_exists => true);

select add_retention_policy('courier_movement', interval '1 months', if_not_exists => true);

select add_dimension('courier_movement', 'courier_id', number_partitions => 1, if_not_exists => true);

create index if not exists idx_cm_courier_id_time on courier_movement (courier_id, time desc);