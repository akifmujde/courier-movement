package com.migros.couriermovement;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CourierMovementApplication {

	public static void main(String[] args) {
		SpringApplication.run(CourierMovementApplication.class, args);
	}

}
