package com.migros.couriermovement.domain.couriermovement.port.driven.db.command;

import com.migros.couriermovement.domain.couriermovement.usecase.CreateCourierMovement;

public interface CourierMovementCommandPort {

    void create(CreateCourierMovement courierMovement);
}
