package com.migros.couriermovement.domain.couriermovement.port.driver.consumer;

import com.migros.couriermovement.domain.couriermovement.usecase.CreateCourierMovement;

public interface CreateCourierMovementConsumer {

    void createCourierMovementConsumer(CreateCourierMovement createCourierMovement);
}
