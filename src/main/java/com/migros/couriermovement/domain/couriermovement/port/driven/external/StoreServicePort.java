package com.migros.couriermovement.domain.couriermovement.port.driven.external;

import java.util.List;

public interface StoreServicePort {

    List<Long> getStoreIdsByLocation(Double lat, Double lon);
}
