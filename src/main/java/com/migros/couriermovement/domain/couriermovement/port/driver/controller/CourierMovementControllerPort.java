package com.migros.couriermovement.domain.couriermovement.port.driver.controller;

public interface CourierMovementControllerPort {

    Double getTotalTraveledDistanceOfCourier(Long courierId);
}
