package com.migros.couriermovement.domain.couriermovement;

import com.migros.couriermovement.domain.couriermovement.port.driven.db.command.CourierStoreLogCommandPort;
import com.migros.couriermovement.domain.couriermovement.port.driven.db.query.CourierStoreLogQueryPort;
import com.migros.couriermovement.domain.couriermovement.port.driven.external.StoreServicePort;
import com.migros.couriermovement.domain.couriermovement.usecase.CreateCourierMovement;
import com.migros.couriermovement.domain.couriermovement.usecase.CreateCourierStoreLog;

import java.util.Date;

public class CourierStoreLogCreateUseCase {

    private static final int DEFAULT_AVAILABLE_TIME_THRESHOLD = 60;

    private final StoreServicePort storeServicePort;
    private final CourierStoreLogQueryPort courierStoreLogQueryPort;
    private final CourierStoreLogCommandPort courierStoreLogCommandPort;

    public CourierStoreLogCreateUseCase(StoreServicePort storeServicePort, CourierStoreLogQueryPort courierStoreLogQueryPort, CourierStoreLogCommandPort courierStoreLogCommandPort) {
        this.storeServicePort = storeServicePort;
        this.courierStoreLogQueryPort = courierStoreLogQueryPort;
        this.courierStoreLogCommandPort = courierStoreLogCommandPort;
    }

    public void addCourierStoreLog(CreateCourierMovement createCourierMovement) {
        storeServicePort.getStoreIdsByLocation(createCourierMovement.lat(), createCourierMovement.lon()).stream()
                .filter(storeId -> isProperForNextLog(storeId, createCourierMovement.courierId(), createCourierMovement.time()))
                .forEach(storeId -> courierStoreLogCommandPort.create(new CreateCourierStoreLog(createCourierMovement.courierId(), storeId, createCourierMovement.lat(), createCourierMovement.lon(), createCourierMovement.time())));
    }

    private boolean isProperForNextLog(Long storeId, Long courierId, Date newLogDate) {
        long startDateEpoc = newLogDate.toInstant().minusSeconds(DEFAULT_AVAILABLE_TIME_THRESHOLD).toEpochMilli();
        return !courierStoreLogQueryPort.existsByCourierIdAndStoreIdAndBetweenStartDateAndEndDate(courierId, storeId, new Date(startDateEpoc), newLogDate);
    }
}
