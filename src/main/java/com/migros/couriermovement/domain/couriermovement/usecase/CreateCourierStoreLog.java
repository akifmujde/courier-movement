package com.migros.couriermovement.domain.couriermovement.usecase;

import java.util.Date;

public record CreateCourierStoreLog (
        Long courierId,
        Long storeId,
        Double lat,
        Double lon,
        Date time
){ }
