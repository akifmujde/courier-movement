package com.migros.couriermovement.domain.couriermovement.model;

import java.util.Date;

public record CourierMovement(
        Long id,
        Long courierId,
        Double lat,
        Double lon,
        Date time,
        Date createdDate
) { }