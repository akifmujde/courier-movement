package com.migros.couriermovement.domain.couriermovement.port.driven.db.query;

import java.util.Optional;

public interface CourierMovementQueryPort {

    Optional<Double> getTotalTraveledDistance(Long courierId);
}
