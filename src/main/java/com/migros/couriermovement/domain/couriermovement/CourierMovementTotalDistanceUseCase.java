package com.migros.couriermovement.domain.couriermovement;

import com.migros.couriermovement.domain.couriermovement.port.driven.db.query.CourierMovementQueryPort;

public class CourierMovementTotalDistanceUseCase {

    private final CourierMovementQueryPort courierMovementQueryPort;

    public CourierMovementTotalDistanceUseCase(CourierMovementQueryPort courierMovementQueryPort) {
        this.courierMovementQueryPort = courierMovementQueryPort;
    }

    public Double getCourierMovementTotalDistance(Long courierId) {
        return courierMovementQueryPort.getTotalTraveledDistance(courierId)
                .orElse(0D);
    }
}
