package com.migros.couriermovement.domain.couriermovement.port.driven.db.command;

import com.migros.couriermovement.domain.couriermovement.usecase.CreateCourierStoreLog;

public interface CourierStoreLogCommandPort {

    void create(CreateCourierStoreLog createCourierStoreLog);
}
