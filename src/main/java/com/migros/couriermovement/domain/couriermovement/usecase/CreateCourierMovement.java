package com.migros.couriermovement.domain.couriermovement.usecase;

import java.util.Date;

public record CreateCourierMovement(
        Long courierId,
        Double lat,
        Double lon,
        Date time
) { }
