package com.migros.couriermovement.domain.couriermovement.port.driven.db.query;

import java.util.Date;

public interface CourierStoreLogQueryPort {

    boolean existsByCourierIdAndStoreIdAndBetweenStartDateAndEndDate(Long courierId, Long storeId, Date startDate, Date endDate);
}
