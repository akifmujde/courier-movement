package com.migros.couriermovement.domain.couriermovement;

import com.migros.couriermovement.domain.couriermovement.port.driven.db.command.CourierMovementCommandPort;
import com.migros.couriermovement.domain.couriermovement.usecase.CreateCourierMovement;

public class CourierMovementCreateUseCase {

    private final CourierMovementCommandPort courierMovementCommandPort;
    private final CourierStoreLogCreateUseCase courierStoreLogCreateUseCase;

    public CourierMovementCreateUseCase(CourierMovementCommandPort courierMovementCommandPort, CourierStoreLogCreateUseCase courierStoreLogCreateUseCase) {
        this.courierMovementCommandPort = courierMovementCommandPort;
        this.courierStoreLogCreateUseCase = courierStoreLogCreateUseCase;
    }

    public void create(CreateCourierMovement createCourierMovement) {
        courierMovementCommandPort.create(createCourierMovement);
        courierStoreLogCreateUseCase.addCourierStoreLog(createCourierMovement);
    }
}
