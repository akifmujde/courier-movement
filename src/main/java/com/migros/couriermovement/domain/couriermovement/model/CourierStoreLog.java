package com.migros.couriermovement.domain.couriermovement.model;

import java.util.Date;

public record CourierStoreLog(
        Long id,
        Long courierId,
        Long storeId,
        Double lat,
        Double lon,
        Date time,
        Date createdDate
) { }
