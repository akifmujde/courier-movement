package com.migros.couriermovement.domain.store;

import com.migros.couriermovement.domain.store.model.Store;
import com.migros.couriermovement.domain.store.port.db.query.StoreQueryPort;
import com.migros.couriermovement.domain.store.usecase.GetStoreByRadius;
import lombok.RequiredArgsConstructor;

import java.util.List;

@RequiredArgsConstructor
public class StoreGetByRadiusUseCase {

    private static final Integer DEFAULT_STORE_RADIUS_THRESHOLD_METERS = 100;

    private final StoreQueryPort storeQueryPort;

    public List<Long> getStoreIdsByLocationIsInTheRadius(GetStoreByRadius getStoreByRadius) {
        return storeQueryPort.getStoresByLocationAndRadius(getStoreByRadius.lat(), getStoreByRadius.lon(), DEFAULT_STORE_RADIUS_THRESHOLD_METERS).stream()
                .map(Store::id)
                .toList();
    }
}
