package com.migros.couriermovement.domain.store.usecase;

public record GetStoreByRadius(
        Double lat,
        Double lon
) { }
