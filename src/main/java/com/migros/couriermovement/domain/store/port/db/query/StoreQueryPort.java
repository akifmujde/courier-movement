package com.migros.couriermovement.domain.store.port.db.query;

import com.migros.couriermovement.domain.store.model.Store;

import java.util.List;

public interface StoreQueryPort {

    List<Store> getStoresByLocationAndRadius(Double lat, Double lon, Integer radiusBasedMeter);
}
