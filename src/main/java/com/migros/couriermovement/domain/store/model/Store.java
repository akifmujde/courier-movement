package com.migros.couriermovement.domain.store.model;

import java.util.Date;

public record Store(
        Long id,
        String name,
        Double lat,
        Double lon,
        Date createdDate
) { }
