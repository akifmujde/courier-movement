package com.migros.couriermovement.infrastructure.adapter.couriermovement.configuration;

import com.migros.couriermovement.domain.couriermovement.CourierStoreLogCreateUseCase;
import com.migros.couriermovement.infrastructure.adapter.couriermovement.db.CourierStoreLogDBCommandAdapter;
import com.migros.couriermovement.infrastructure.adapter.couriermovement.db.CourierStoreLogDBQueryAdapter;
import com.migros.couriermovement.infrastructure.adapter.couriermovement.external.StoreServiceAdapter;
import lombok.RequiredArgsConstructor;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
@RequiredArgsConstructor
public class CourierStoreLogBeanConfiguration {

    private final StoreServiceAdapter storeServiceAdapter;
    private final CourierStoreLogDBQueryAdapter courierStoreLogDBQueryAdapter;
    private final CourierStoreLogDBCommandAdapter courierStoreLogDBCommandAdapter;

    @Bean
    public CourierStoreLogCreateUseCase courierStoreLogCreateUseCase() {
        return new CourierStoreLogCreateUseCase(storeServiceAdapter, courierStoreLogDBQueryAdapter, courierStoreLogDBCommandAdapter);
    }
}
