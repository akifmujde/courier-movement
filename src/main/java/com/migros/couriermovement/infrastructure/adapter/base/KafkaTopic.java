package com.migros.couriermovement.infrastructure.adapter.base;

public class KafkaTopic {

    private KafkaTopic() {
    }

    public static final String COURIER_LOCATION_POINT = "migros.courier.location-point";
    public static final String COURIER_LOCATION_POINT_DLQ = "migros.courier.location-point.dlq";
}
