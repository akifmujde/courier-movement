package com.migros.couriermovement.infrastructure.adapter.couriermovement.consumer;

import com.migros.couriermovement.domain.couriermovement.CourierMovementCreateUseCase;
import com.migros.couriermovement.domain.couriermovement.port.driver.consumer.CreateCourierMovementConsumer;
import com.migros.couriermovement.domain.couriermovement.usecase.CreateCourierMovement;
import com.migros.couriermovement.infrastructure.adapter.base.KafkaTopic;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Component;

@Component
public class CourierMovementListener implements CreateCourierMovementConsumer {

    private final CourierMovementCreateUseCase courierMovementCreateUseCase;

    public CourierMovementListener(CourierMovementCreateUseCase courierMovementCreateUseCase) {
        this.courierMovementCreateUseCase = courierMovementCreateUseCase;
    }

    @Override
    @KafkaListener(topics = KafkaTopic.COURIER_LOCATION_POINT)
    public void createCourierMovementConsumer(CreateCourierMovement createCourierMovement) {
        courierMovementCreateUseCase.create(createCourierMovement);
    }
}
