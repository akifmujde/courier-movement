package com.migros.couriermovement.infrastructure.adapter.store.db;

import com.migros.couriermovement.infrastructure.adapter.base.BaseRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface StoreRepository extends BaseRepository<StoreEntity> {

    @Query(nativeQuery = true, value = "select * from migros_store z where st_dwithin(geography(st_makepoint(lon, lat)), geography(st_makepoint(?2, ?1)), ?3)")
    List<StoreEntity> getStoresByLocationAndRadius(Double lat, Double lon, Integer radiusBasedMeter);
}