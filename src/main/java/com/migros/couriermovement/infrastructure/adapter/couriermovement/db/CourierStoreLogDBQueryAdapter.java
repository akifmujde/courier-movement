package com.migros.couriermovement.infrastructure.adapter.couriermovement.db;

import com.migros.couriermovement.domain.couriermovement.port.driven.db.query.CourierStoreLogQueryPort;
import com.migros.couriermovement.infrastructure.adapter.couriermovement.db.repository.CourierStoreLogRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Repository;

import java.util.Date;

@Repository
@RequiredArgsConstructor
public class CourierStoreLogDBQueryAdapter implements  CourierStoreLogQueryPort {

    private final CourierStoreLogRepository courierStoreLogRepository;

    @Override
    public boolean existsByCourierIdAndStoreIdAndBetweenStartDateAndEndDate(Long courierId, Long storeId, Date startDate, Date endDate) {
        return courierStoreLogRepository.existsByCourierIdAndStoreIdAndBetweenStartDateAndEndDate(courierId, storeId, startDate, endDate);
    }
}
