package com.migros.couriermovement.infrastructure.adapter.couriermovement.db;

import com.migros.couriermovement.domain.couriermovement.port.driven.db.command.CourierStoreLogCommandPort;
import com.migros.couriermovement.domain.couriermovement.usecase.CreateCourierStoreLog;
import com.migros.couriermovement.infrastructure.adapter.couriermovement.db.entity.CourierStoreLogEntity;
import com.migros.couriermovement.infrastructure.adapter.couriermovement.db.repository.CourierStoreLogRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Repository;

@Repository
@RequiredArgsConstructor
public class CourierStoreLogDBCommandAdapter implements CourierStoreLogCommandPort {

    private final CourierStoreLogRepository courierStoreLogRepository;

    @Override
    public void create(CreateCourierStoreLog createCourierStoreLog) {
        CourierStoreLogEntity courierStoreLogEntity = CourierStoreLogEntity.create(createCourierStoreLog);
        courierStoreLogRepository.save(courierStoreLogEntity);
    }
}
