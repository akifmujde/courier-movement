package com.migros.couriermovement.infrastructure.adapter.couriermovement.db.entity;

import com.migros.couriermovement.domain.couriermovement.usecase.CreateCourierMovement;
import com.migros.couriermovement.infrastructure.adapter.base.BaseEntity;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.SequenceGenerator;
import jakarta.persistence.Table;
import jakarta.persistence.Temporal;
import jakarta.persistence.TemporalType;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.Date;

@Data
@Entity
@Table(name = "courier_movement")
@EqualsAndHashCode(callSuper = true)
@SequenceGenerator(name = "id_generator", sequenceName = "seq_courier_movement")
public class CourierMovementEntity extends BaseEntity {

    @Column(name = "courier_id", nullable = false, unique = true)
    private Long courierId;

    @Column(name = "lat", nullable = false)
    private Double lat;

    @Column(name = "lon", nullable = false)
    private Double lon;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "time", nullable = false)
    private Date time;

    public static CourierMovementEntity create(CreateCourierMovement createCourierMovement) {
        CourierMovementEntity courierMovementEntity = new CourierMovementEntity();
        courierMovementEntity.setCourierId(createCourierMovement.courierId());
        courierMovementEntity.setLat(createCourierMovement.lat());
        courierMovementEntity.setLon(createCourierMovement.lon());
        courierMovementEntity.setTime(createCourierMovement.time());
        return courierMovementEntity;
    }
}


