package com.migros.couriermovement.infrastructure.adapter.couriermovement.external;

import com.migros.couriermovement.domain.couriermovement.port.driven.external.StoreServicePort;
import com.migros.couriermovement.domain.store.StoreGetByRadiusUseCase;
import com.migros.couriermovement.domain.store.usecase.GetStoreByRadius;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
@RequiredArgsConstructor
public class StoreServiceAdapter implements StoreServicePort {

    private final StoreGetByRadiusUseCase storeGetByRadiusUseCase;

    @Override
    public List<Long> getStoreIdsByLocation(Double lat, Double lon) {
        return storeGetByRadiusUseCase.getStoreIdsByLocationIsInTheRadius(new GetStoreByRadius(lat, lon));
    }
}
