package com.migros.couriermovement.infrastructure.adapter.couriermovement.contoller;

import com.migros.couriermovement.domain.couriermovement.CourierMovementTotalDistanceUseCase;
import com.migros.couriermovement.domain.couriermovement.port.driver.controller.CourierMovementControllerPort;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("courier-movement")
public class CourierTotalDistanceRestController implements CourierMovementControllerPort {

    private final CourierMovementTotalDistanceUseCase courierMovementTotalDistanceUseCase;

    public CourierTotalDistanceRestController(CourierMovementTotalDistanceUseCase courierMovementTotalDistanceUseCase) {
        this.courierMovementTotalDistanceUseCase = courierMovementTotalDistanceUseCase;
    }

    @Override
    @GetMapping("{courierId}")
    public Double getTotalTraveledDistanceOfCourier(@PathVariable Long courierId) {
        return courierMovementTotalDistanceUseCase.getCourierMovementTotalDistance(courierId);
    }
}
