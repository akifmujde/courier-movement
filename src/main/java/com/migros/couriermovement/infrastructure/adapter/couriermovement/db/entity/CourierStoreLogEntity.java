package com.migros.couriermovement.infrastructure.adapter.couriermovement.db.entity;

import com.migros.couriermovement.domain.couriermovement.usecase.CreateCourierStoreLog;
import com.migros.couriermovement.infrastructure.adapter.base.BaseEntity;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.SequenceGenerator;
import jakarta.persistence.Table;
import jakarta.persistence.Temporal;
import jakarta.persistence.TemporalType;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.Date;

@Data
@Entity
@Table(name = "courier_store_log")
@EqualsAndHashCode(callSuper = true)
@SequenceGenerator(name = "id_generator", sequenceName = "seq_courier_store_log")
public class CourierStoreLogEntity extends BaseEntity {

    @Column(name = "courier_id", nullable = false)
    private Long courierId;

    @Column(name = "store_id", nullable = false)
    private Long migrosStoreId;

    @Column(name = "lat", nullable = false)
    private Double lat;

    @Column(name = "lon", nullable = false)
    private Double lon;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "time", nullable = false)
    private Date time;

    public static CourierStoreLogEntity create(CreateCourierStoreLog createCourierStoreLog) {
        CourierStoreLogEntity courierStoreLogEntity = new CourierStoreLogEntity();
        courierStoreLogEntity.setMigrosStoreId(createCourierStoreLog.storeId());
        courierStoreLogEntity.setCourierId(createCourierStoreLog.courierId());
        courierStoreLogEntity.setLat(createCourierStoreLog.lat());
        courierStoreLogEntity.setLon(createCourierStoreLog.lon());
        courierStoreLogEntity.setTime(createCourierStoreLog.time());
        return courierStoreLogEntity;
    }
}
