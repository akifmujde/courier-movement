package com.migros.couriermovement.infrastructure.adapter.store.configuration;

import com.migros.couriermovement.domain.store.StoreGetByRadiusUseCase;
import com.migros.couriermovement.infrastructure.adapter.store.db.StoreDBQueryAdapter;
import lombok.RequiredArgsConstructor;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
@RequiredArgsConstructor
public class StoreBeanConfiguration {

    private final StoreDBQueryAdapter storeDBQueryAdapter;

    @Bean
    public StoreGetByRadiusUseCase storeGetByRadiusUseCase() {
        return new StoreGetByRadiusUseCase(storeDBQueryAdapter);
    }
}
