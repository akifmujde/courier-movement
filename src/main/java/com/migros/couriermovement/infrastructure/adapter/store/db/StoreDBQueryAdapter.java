package com.migros.couriermovement.infrastructure.adapter.store.db;

import com.migros.couriermovement.domain.store.model.Store;
import com.migros.couriermovement.domain.store.port.db.query.StoreQueryPort;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
@RequiredArgsConstructor
public class StoreDBQueryAdapter implements StoreQueryPort {

    private final StoreRepository storeRepository;

    @Override
    public List<Store> getStoresByLocationAndRadius(Double lat, Double lon, Integer radiusBasedMeter) {
        return storeRepository.getStoresByLocationAndRadius(lat, lon, radiusBasedMeter).stream()
                .map(StoreDBQueryAdapter::generateStore)
                .toList();
    }

    private static Store generateStore(StoreEntity storeEntity) {
        return new Store(storeEntity.getId(),
                storeEntity.getName(),
                storeEntity.getLat(),
                storeEntity.getLon(),
                storeEntity.getCreatedDate());
    }
}
