package com.migros.couriermovement.infrastructure.adapter.couriermovement.db;

import com.migros.couriermovement.domain.couriermovement.port.driven.db.command.CourierMovementCommandPort;
import com.migros.couriermovement.domain.couriermovement.usecase.CreateCourierMovement;
import com.migros.couriermovement.infrastructure.adapter.couriermovement.db.entity.CourierMovementEntity;
import com.migros.couriermovement.infrastructure.adapter.couriermovement.db.repository.CourierMovementRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Repository;

@Repository
@RequiredArgsConstructor
public class CourierMovementDBCommandAdapter implements CourierMovementCommandPort {

    private final CourierMovementRepository courierMovementRepository;

    @Override
    public void create(CreateCourierMovement createCourierMovement) {
        CourierMovementEntity courierMovementEntity = CourierMovementEntity.create(createCourierMovement);
        courierMovementRepository.save(courierMovementEntity);
    }
}
