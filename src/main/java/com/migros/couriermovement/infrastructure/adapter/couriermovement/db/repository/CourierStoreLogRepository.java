package com.migros.couriermovement.infrastructure.adapter.couriermovement.db.repository;

import com.migros.couriermovement.infrastructure.adapter.base.BaseRepository;
import com.migros.couriermovement.infrastructure.adapter.couriermovement.db.entity.CourierStoreLogEntity;
import org.springframework.data.jpa.repository.Query;

import java.util.Date;
import java.util.Optional;

public interface CourierStoreLogRepository extends BaseRepository<CourierStoreLogEntity> {

    Optional<CourierStoreLogEntity> findFirstByCourierIdAndMigrosStoreIdOrderByTimeDesc(Long courierId, Long migrosStoreId);

    @Query(nativeQuery = true, value = "select exists(select 1 from courier_store_log where courier_id = ?1 and store_id = ?2 and time >= ?3 and time < ?4)")
    boolean existsByCourierIdAndStoreIdAndBetweenStartDateAndEndDate(Long courierId, Long storeId, Date startDate, Date endDate);
}