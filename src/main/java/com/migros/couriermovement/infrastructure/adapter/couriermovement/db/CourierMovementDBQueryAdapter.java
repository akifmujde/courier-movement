package com.migros.couriermovement.infrastructure.adapter.couriermovement.db;

import com.migros.couriermovement.domain.couriermovement.port.driven.db.query.CourierMovementQueryPort;
import com.migros.couriermovement.infrastructure.adapter.couriermovement.db.repository.CourierMovementRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
@RequiredArgsConstructor
public class CourierMovementDBQueryAdapter implements CourierMovementQueryPort {

    private final CourierMovementRepository courierMovementRepository;

    @Override
    public Optional<Double> getTotalTraveledDistance(Long courierId) {
        return courierMovementRepository.getTotalTraveledDistance(courierId);
    }
}
