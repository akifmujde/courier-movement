package com.migros.couriermovement.infrastructure.adapter.couriermovement.db.repository;

import com.migros.couriermovement.infrastructure.adapter.base.BaseRepository;
import com.migros.couriermovement.infrastructure.adapter.couriermovement.db.entity.CourierMovementEntity;
import org.springframework.data.jpa.repository.Query;

import java.util.Optional;

public interface CourierMovementRepository extends BaseRepository<CourierMovementEntity> {

    @Query(nativeQuery = true, value = "SELECT st_length(geography(st_makeline(location))) as distance FROM (select courier_id, st_makepoint(lon, lat) as location from courier_movement) t where courier_id = ?1")
    Optional<Double> getTotalTraveledDistance(Long courierId);
}