package com.migros.couriermovement.infrastructure.adapter.couriermovement.configuration;

import com.migros.couriermovement.domain.couriermovement.CourierMovementCreateUseCase;
import com.migros.couriermovement.domain.couriermovement.CourierMovementTotalDistanceUseCase;
import com.migros.couriermovement.domain.couriermovement.CourierStoreLogCreateUseCase;
import com.migros.couriermovement.infrastructure.adapter.couriermovement.db.CourierMovementDBCommandAdapter;
import com.migros.couriermovement.infrastructure.adapter.couriermovement.db.CourierMovementDBQueryAdapter;
import lombok.RequiredArgsConstructor;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
@RequiredArgsConstructor
public class CourierMovementBeanConfiguration {

    private final CourierStoreLogCreateUseCase courierStoreLogCreateUseCase;
    private final CourierMovementDBQueryAdapter courierMovementDBQueryAdapter;
    private final CourierMovementDBCommandAdapter courierMovementDBCommandAdapter;

    @Bean
    public CourierMovementCreateUseCase courierMovementCreateUseCase() {
        return new CourierMovementCreateUseCase(courierMovementDBCommandAdapter, courierStoreLogCreateUseCase);
    }

    @Bean
    public CourierMovementTotalDistanceUseCase courierMovementTotalDistanceUseCase() {
        return new CourierMovementTotalDistanceUseCase(courierMovementDBQueryAdapter);
    }
}
