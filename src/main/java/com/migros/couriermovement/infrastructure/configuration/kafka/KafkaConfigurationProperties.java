package com.migros.couriermovement.infrastructure.configuration.kafka;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

@Data
@Configuration
@ConfigurationProperties(prefix = "migros.kafka")
public class KafkaConfigurationProperties {

    private String brokers;
    private String groupId;
}
