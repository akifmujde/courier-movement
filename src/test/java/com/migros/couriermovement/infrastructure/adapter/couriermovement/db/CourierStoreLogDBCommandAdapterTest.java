package com.migros.couriermovement.infrastructure.adapter.couriermovement.db;

import com.migros.couriermovement.domain.base.BaseMockitoTest;
import com.migros.couriermovement.domain.couriermovement.usecase.CreateCourierStoreLog;
import com.migros.couriermovement.infrastructure.adapter.couriermovement.db.entity.CourierStoreLogEntity;
import com.migros.couriermovement.infrastructure.adapter.couriermovement.db.repository.CourierStoreLogRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.Mock;

import java.util.Date;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.mockito.Mockito.verify;

class CourierStoreLogDBCommandAdapterTest extends BaseMockitoTest {

    @Mock
    private CourierStoreLogRepository courierStoreLogRepository;

    private CourierStoreLogDBCommandAdapter courierStoreLogDBCommandAdapter;

    @Captor
    private ArgumentCaptor<CourierStoreLogEntity> courierStoreLogEntityArgumentCaptor;

    @BeforeEach
    void setUp() {
        courierStoreLogDBCommandAdapter = new CourierStoreLogDBCommandAdapter(courierStoreLogRepository);
    }

    @Test
    void shouldCreateNewCourierStoreLog() {
        CreateCourierStoreLog createCourierStoreLog = new CreateCourierStoreLog(1L, 13L, 11d, 12d, new Date());

        courierStoreLogDBCommandAdapter.create(createCourierStoreLog);

        verify(courierStoreLogRepository).save(courierStoreLogEntityArgumentCaptor.capture());

        CourierStoreLogEntity savedCourierStoreLogEntity = courierStoreLogEntityArgumentCaptor.getValue();

        assertNull(savedCourierStoreLogEntity.getId());
        assertThat(savedCourierStoreLogEntity.getCourierId(), equalTo(createCourierStoreLog.courierId()));
        assertThat(savedCourierStoreLogEntity.getMigrosStoreId(), equalTo(createCourierStoreLog.storeId()));
        assertThat(savedCourierStoreLogEntity.getLat(), equalTo(createCourierStoreLog.lat()));
        assertThat(savedCourierStoreLogEntity.getLon(), equalTo(createCourierStoreLog.lon()));
        assertThat(savedCourierStoreLogEntity.getTime(), equalTo(createCourierStoreLog.time()));
        assertNull(savedCourierStoreLogEntity.getCreatedDate());
    }
}