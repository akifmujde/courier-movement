package com.migros.couriermovement.infrastructure.adapter.couriermovement.db;

import com.migros.couriermovement.domain.base.BaseMockitoTest;
import com.migros.couriermovement.domain.couriermovement.usecase.CreateCourierMovement;
import com.migros.couriermovement.infrastructure.adapter.couriermovement.db.entity.CourierMovementEntity;
import com.migros.couriermovement.infrastructure.adapter.couriermovement.db.repository.CourierMovementRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.Mock;

import java.util.Date;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.mockito.Mockito.verify;

class CourierMovementDBCommandAdapterTest extends BaseMockitoTest {

    @Mock
    private CourierMovementRepository courierMovementRepository;

    private CourierMovementDBCommandAdapter courierMovementDBCommandAdapter;

    @Captor
    private ArgumentCaptor<CourierMovementEntity> courierMovementEntityArgumentCaptor;

    @BeforeEach
    void setUp() {
        courierMovementDBCommandAdapter = new CourierMovementDBCommandAdapter(courierMovementRepository);
    }

    @Test
    void shouldCreateNewCourierMovement() {
        CreateCourierMovement createCourierMovement = new CreateCourierMovement(1L, 10d, 11d, new Date());

        courierMovementDBCommandAdapter.create(createCourierMovement);

        verify(courierMovementRepository).save(courierMovementEntityArgumentCaptor.capture());

        CourierMovementEntity savedCourierMovementEntity = courierMovementEntityArgumentCaptor.getValue();

        assertNull(savedCourierMovementEntity.getId());
        assertThat(savedCourierMovementEntity.getCourierId(), equalTo(createCourierMovement.courierId()));
        assertThat(savedCourierMovementEntity.getLat(), equalTo(createCourierMovement.lat()));
        assertThat(savedCourierMovementEntity.getLon(), equalTo(createCourierMovement.lon()));
        assertThat(savedCourierMovementEntity.getTime(), equalTo(createCourierMovement.time()));
        assertNull(savedCourierMovementEntity.getCreatedDate());
    }
}