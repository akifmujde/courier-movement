package com.migros.couriermovement.domain.store;

import com.migros.couriermovement.domain.base.BaseMockitoTest;
import com.migros.couriermovement.domain.store.model.Store;
import com.migros.couriermovement.domain.store.port.db.query.StoreQueryPort;
import com.migros.couriermovement.domain.store.usecase.GetStoreByRadius;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;

import java.util.Collections;
import java.util.Date;
import java.util.List;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.hasSize;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

class StoreGetByRadiusUseCaseTest extends BaseMockitoTest {

    @Mock
    private StoreQueryPort storeQueryPort;

    private StoreGetByRadiusUseCase storeGetByRadiusUseCase;

    @BeforeEach
    void setUp() {
        storeGetByRadiusUseCase = new StoreGetByRadiusUseCase(storeQueryPort);
    }

    @Test
    void shouldGetStoreIdsByLocationIsInTheRadius() {
        GetStoreByRadius getStoreByRadius = new GetStoreByRadius(40d, 41d);

        Store migros = new Store(1L, "Migros", 40.2d, 41.2d, new Date());

        when(storeQueryPort.getStoresByLocationAndRadius(getStoreByRadius.lat(), getStoreByRadius.lon(), 100)).thenReturn(Collections.singletonList(migros));

        List<Long> storeIds = storeGetByRadiusUseCase.getStoreIdsByLocationIsInTheRadius(getStoreByRadius);

        verify(storeQueryPort).getStoresByLocationAndRadius(getStoreByRadius.lat(), getStoreByRadius.lon(), 100);

        assertThat(storeIds, hasSize(1));
        assertThat(storeIds.get(0), equalTo(migros.id()));
    }
}