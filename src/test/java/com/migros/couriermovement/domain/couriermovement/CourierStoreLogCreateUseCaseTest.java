package com.migros.couriermovement.domain.couriermovement;

import com.migros.couriermovement.domain.base.BaseMockitoTest;
import com.migros.couriermovement.domain.couriermovement.port.driven.db.command.CourierStoreLogCommandPort;
import com.migros.couriermovement.domain.couriermovement.port.driven.db.query.CourierStoreLogQueryPort;
import com.migros.couriermovement.domain.couriermovement.port.driven.external.StoreServicePort;
import com.migros.couriermovement.domain.couriermovement.usecase.CreateCourierMovement;
import com.migros.couriermovement.domain.couriermovement.usecase.CreateCourierStoreLog;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.Mock;

import java.util.Collections;
import java.util.Date;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoInteractions;
import static org.mockito.Mockito.when;

class CourierStoreLogCreateUseCaseTest extends BaseMockitoTest {

    @Mock
    private StoreServicePort storeServicePort;

    @Mock
    private CourierStoreLogQueryPort courierStoreLogQueryPort;

    @Mock
    private CourierStoreLogCommandPort courierStoreLogCommandPort;

    private CourierStoreLogCreateUseCase courierStoreLogCreateUseCase;

    @Captor
    private ArgumentCaptor<CreateCourierStoreLog> createCourierStoreLogArgumentCaptor;

    @BeforeEach
    void setUp() {
        courierStoreLogCreateUseCase = new CourierStoreLogCreateUseCase(storeServicePort, courierStoreLogQueryPort, courierStoreLogCommandPort);
    }

    @Test
    void shouldAddCourierStoreLog() {
        long courierId = 1;
        long storeId = 13;
        double lat = 11;
        double lon = 12;
        Date nextLogDate = new Date();
        Date startDate = new Date(nextLogDate.toInstant().minusSeconds(60).toEpochMilli());

        CreateCourierMovement createCourierMovement = new CreateCourierMovement(courierId, lat, lon, nextLogDate);

        when(storeServicePort.getStoreIdsByLocation(lat, lon)).thenReturn(Collections.singletonList(storeId));
        when(courierStoreLogQueryPort.existsByCourierIdAndStoreIdAndBetweenStartDateAndEndDate(courierId, storeId, startDate, nextLogDate)).thenReturn(false);

        courierStoreLogCreateUseCase.addCourierStoreLog(createCourierMovement);

        verify(storeServicePort).getStoreIdsByLocation(lat, lon);
        verify(courierStoreLogQueryPort).existsByCourierIdAndStoreIdAndBetweenStartDateAndEndDate(courierId, storeId, startDate, nextLogDate);
        verify(courierStoreLogCommandPort).create(createCourierStoreLogArgumentCaptor.capture());

        CreateCourierStoreLog newCourierStoreLog = createCourierStoreLogArgumentCaptor.getValue();

        assertThat(newCourierStoreLog.courierId(), equalTo(courierId));
        assertThat(newCourierStoreLog.storeId(), equalTo(storeId));
        assertThat(newCourierStoreLog.lat(), equalTo(lat));
        assertThat(newCourierStoreLog.lon(), equalTo(lon));
        assertThat(newCourierStoreLog.time(), equalTo(nextLogDate));
    }

    @Test
    void shouldNotAddCourierStoreLogWhenThereIsALogInLastOneMin() {
        long courierId = 1;
        long storeId = 13;
        double lat = 11;
        double lon = 12;
        Date nextLogDate = new Date();
        Date startDate = new Date(nextLogDate.toInstant().minusSeconds(60).toEpochMilli());

        CreateCourierMovement createCourierMovement = new CreateCourierMovement(courierId, lat, lon, nextLogDate);

        when(storeServicePort.getStoreIdsByLocation(lat, lon)).thenReturn(Collections.singletonList(storeId));
        when(courierStoreLogQueryPort.existsByCourierIdAndStoreIdAndBetweenStartDateAndEndDate(courierId, storeId, startDate, nextLogDate)).thenReturn(true);

        courierStoreLogCreateUseCase.addCourierStoreLog(createCourierMovement);

        verify(storeServicePort).getStoreIdsByLocation(lat, lon);
        verify(courierStoreLogQueryPort).existsByCourierIdAndStoreIdAndBetweenStartDateAndEndDate(courierId, storeId, startDate, nextLogDate);
        verifyNoInteractions(courierStoreLogCommandPort);
    }
}