package com.migros.couriermovement.domain.couriermovement;

import com.migros.couriermovement.domain.base.BaseMockitoTest;
import com.migros.couriermovement.domain.couriermovement.port.driven.db.command.CourierMovementCommandPort;
import com.migros.couriermovement.domain.couriermovement.usecase.CreateCourierMovement;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.Mock;

import java.util.Date;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.mockito.Mockito.verify;


class CourierMovementCreateUseCaseTest extends BaseMockitoTest {

    @Mock
    private CourierMovementCommandPort courierMovementCommandPort;

    @Mock
    private CourierStoreLogCreateUseCase courierStoreLogCreateUseCase;

    private CourierMovementCreateUseCase courierMovementCreateUseCase;

    @Captor
    private ArgumentCaptor<CreateCourierMovement> createCourierMovementArgumentCaptor;

    @Captor
    private ArgumentCaptor<CreateCourierMovement> addCourierStoreLogArgumentCaptor;

    @BeforeEach
    void setUp() {
        courierMovementCreateUseCase = new CourierMovementCreateUseCase(courierMovementCommandPort, courierStoreLogCreateUseCase);
    }

    @Test
    void shouldCreateCourierMovement() {
        long courierId = 1;
        double lat = 41;
        double lon = 41;
        Date time = new Date();

        CreateCourierMovement createCourierMovement = new CreateCourierMovement(courierId, lat, lon, time);

        courierMovementCreateUseCase.create(createCourierMovement);

        verify(courierMovementCommandPort).create(createCourierMovementArgumentCaptor.capture());
        verify(courierStoreLogCreateUseCase).addCourierStoreLog(addCourierStoreLogArgumentCaptor.capture());

        assertThat(createCourierMovement, equalTo(addCourierStoreLogArgumentCaptor.getValue()));
        assertThat(createCourierMovement, equalTo(createCourierMovementArgumentCaptor.getValue()));
    }
}