package com.migros.couriermovement.domain.couriermovement;

import com.migros.couriermovement.domain.base.BaseMockitoTest;
import com.migros.couriermovement.domain.couriermovement.port.driven.db.query.CourierMovementQueryPort;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;

import java.util.Optional;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

class CourierMovementTotalDistanceUseCaseTest extends BaseMockitoTest {

    @Mock
    private CourierMovementQueryPort courierMovementQueryPort;

    private CourierMovementTotalDistanceUseCase courierMovementTotalDistanceUseCase;

    @BeforeEach
    void setUp() {
        courierMovementTotalDistanceUseCase = new CourierMovementTotalDistanceUseCase(courierMovementQueryPort);
    }

    @Test
    void shouldGetCourierMovementTotalDistance() {
        long courierId = 1L;
        when(courierMovementQueryPort.getTotalTraveledDistance(courierId)).thenReturn(Optional.of(12D));

        Double courierMovementTotalDistance = courierMovementTotalDistanceUseCase.getCourierMovementTotalDistance(courierId);

        verify(courierMovementQueryPort).getTotalTraveledDistance(courierId);

        assertThat(courierMovementTotalDistance, equalTo(12D));
    }

    @Test
    void shouldGetZeroWhenTotalDistanceDoesNotExist() {
        long courierId = 1L;
        when(courierMovementQueryPort.getTotalTraveledDistance(courierId)).thenReturn(Optional.empty());

        Double courierMovementTotalDistance = courierMovementTotalDistanceUseCase.getCourierMovementTotalDistance(courierId);

        verify(courierMovementQueryPort).getTotalTraveledDistance(courierId);

        assertThat(courierMovementTotalDistance, equalTo(0D));
    }
}